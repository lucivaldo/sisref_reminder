// Extensão do Google Chrome para lembrar o servidor de registrar a frequência no SISREF.
// Author: Lucivaldo Castro Castelo Branco
// Email: lucivaldocb@gmail.com

const reminderServerUrl = 'https://sisref-reminder-server.herokuapp.com'
const timeUrl = reminderServerUrl + '/servertime'
const sisrefInfoUrl = reminderServerUrl + '/sisrefinfo'
const sisrefUrl = 'https://sisref.defensoria.to.def.br'
const alarmName = 'sisref.alarm'

const periodList = {
  beforeFirst: 'beforeFirst',
  first: 'first',
  beforeSecond: 'beforeSecond',
  second: 'second',
  beforeThird: 'beforeThird',
  third: 'third',
  beforeFourth: 'beforeFourth',
  fourth: 'fourth'
}

const statusList = {
  AGUARDANDO: 'AGUARDANDO',
  CONSISTENTE: 'CONSISTENTE',
  INCONSISTENTE: 'INCONSISTENTE',
  AVALIACAO: 'EM AVALIAÇÃO'
}

// Adiciona instruções a serem executadas quando a extensão for instalada
// ou atualizada.
chrome.runtime.onInstalled.addListener (details => {
  // Apenas exibe uma mensagem no console quando a extensão for instalada
  // ou atualizada mostrando o nome da extensão e a sua versão.
  console.info('Extensão %s, versão %s instalada/atualizada com sucesso.',
    chrome.runtime.getManifest().name, chrome.runtime.getManifest().version
  )

  // createAlarm(1, 1)
})

function createAlarm(delay, period) {
  chrome.alarms.create(alarmName, {
    delayInMinutes: delay,
    periodInMinutes: period
  })
}

chrome.alarms.onAlarm.addListener(alarm => {
  if (alarm.name != alarmName) return

  return

  getSessionId(async sessionid => {
    const response = await getTimeInfo(sessionid, navigator.userAgent)
    const data = new Date(response.data)
    
    if (!isDate(data)) {
      console.log('Dia da semana não permitido para fazer a verificação.')
      modifyAlarm(data)
      return
    }

    console.log('Dia da semana permitido para fazer a verificação.')

    const period = checkTime(data)
    console.log('period:', period)

    if (period == null) {
      console.log('Horário não permitido para fazer a verificação')
      modifyAlarm(data)
      return
    }

    const registros = await getSisrefInfo(sessionid, navigator.userAgent)
    const status = checkStatus(registros.registros, period)
    console.log('status:', status)

    if (status !== statusList.AGUARDANDO) {
      console.log('O alarma será modificado para despertar em 1 hora')
      createAlarm(60, 1)
      return
    }

    if (period === periodList.beforeFirst || period === periodList.beforeThird) {
      console.log('Notificação para lembrar o servidor que já pode registrar a frequência.')
      createNotificationRegistrationPermission()
      createAlarm(null, 3.5)
    } else if (period === periodList.beforeSecond || period === periodList.beforeFourth) {
      console.log('Notificação para lembrar o servidor de registrar a frequência antes de sair.')
      createNotificationReminder()
      createAlarm(null, 3.5)
    } else {
      console.log('Notificação para alertar o servidor de registrar a frequência.')
      createNotificationAlert()
      createAlarm(null, 1)
    }
  })
})

function getSessionId(cb) {
  chrome.cookies.get({ url: sisrefUrl, name: 'sessionid' }, cookie => {
    cb(cookie.value)
  })
}

async function getTimeInfo(sessionid, useragent) {
  try {
    const headers = { 'Accept': 'applicaton/json', 'Content-Type': 'application/json' }
    const body = JSON.stringify({ sessionid, useragent })

    const response = await fetch(timeUrl, { method: 'post', headers, body })
    return await response.json()
  } catch (error) {
    console.log(error)
    return null
  }
}

async function getSisrefInfo(sessionid, useragent) {
  try {
    const headers = { 'Accept': 'applicaton/json', 'Content-Type': 'application/json' }
    const body = JSON.stringify({ sessionid, useragent })

    const response = await fetch(sisrefInfoUrl, { method: 'post', headers, body })
    return await response.json()
  } catch (error) {
    console.log(error)
    return null
  }
}

function isDate(date) {
  var daysAllowed = [false, true, true, true, true, true, false]
  return daysAllowed[date.getDay()]
}

function checkTime(date) {
  // Para debug.
  // return periodList.fourth

  var hours = date.getHours()
  var minutes = date.getMinutes()

  if (hours == 7 && minutes >= 45) {
    return periodList.beforeFirst
  } else if (hours == 8 && minutes < 15) {
    return periodList.first
  } else if (hours == 11 && minutes >= 50) {
    return periodList.beforeSecond
  } else if (hours == 12 && minutes < 15) {
    return periodList.second
  } else if (hours == 13 && minutes >= 45) {
    return periodList.beforeThird
  } else if (hours == 14 && minutes < 15) {
    return periodList.third
  } else if (hours == 16 && minutes >= 50) {
    return periodList.beforeFourth
  } else if (hours == 17 && minutes < 15) {
    return periodList.fourth
  } else {
    return null
  }
}

function checkStatus(registros, period) {
  // Para debug.
  // return statusList.AGUARDANDO

  let status = null

  if (period == periodList.first || period == periodList.beforeFirst) {
    status = registros[0]
  } else if (period == periodList.second || period == periodList.beforeSecond) {
    status = registros[1]
  } else if (period == periodList.third || period == periodList.beforeThird) {
    status = registros[2]
  } else if (period == periodList.fourth || period == periodList.beforeFourth) {
    status = registros[3]
  }

  if (status != null) {
    return status[4]
  } else {
    return null
  }
}

function modifyAlarm(date) {
  var dayOfWeek = date.getDay()
  var hours = date.getHours()
  var minutes = date.getMinutes()

  var sleepHours
  var sleepMinutes
  var sleep

  if (isDate(date)) {
    if (hours < 8) {
      console.info("Horas menor que 8: %s", date)
      sleepHours = 8 - hours - 1
      sleepMinutes = 60 - minutes - 15
    } else if (hours < 12) {
      console.info("Horas menor que 12: %s", date)
      sleepHours = 12 - hours - 1
      sleepMinutes = 60 - minutes - 5
    } else if (hours < 14) {
      console.info("Horas menor que 14: %s", date)
      sleepHours = 14 - hours - 1
      sleepMinutes = 60 - minutes - 15
    } else if (hours < 17) {
      console.info("Horas menor que 17: %s", date)
      sleepHours = 17 - hours - 1
      sleepMinutes = 60 - minutes - 5
    } else if (hours < 23) {
      console.info("Horas menor que 23: %s", date)
      sleepHours = 23 - hours + 8
      sleepMinutes = 60 - minutes - 15
    }
  } else {
    if (dayOfWeek == 0) {
      console.info("Domingo: %s", date)
      sleepHours = 23 - hours + 8
      sleepMinutes = 60 - minutes - 15
    } else if (dayOfWeek == 6) {
      console.info("Sábado: %s", date)
      sleepHours = 24 * 2 - hours + 7
      sleepMinutes = 60 - minutes - 15
    }
  }

  sleep = sleepHours * 60 + sleepMinutes

  createAlarm(sleep, 1)

  console.info("O alarme %s foi modificado. Ele será disparado novamente em %d horas e %d minutos. Hora atual: %dh%dm",
    alarmName, Math.round(sleep / 60), (sleep % 60), hours, minutes)
}

function createNotificationRegistrationPermission () {
  chrome.notifications.create('notification.sisref.registrationPermission', {
    type: 'basic',
    title: 'Lembrete para o SISREF',
    message: 'Você já pode efetuar o registro de frequência no SISREF.',
    iconUrl: 'alert.png'
  })
}

function createNotificationAlert () {
  chrome.notifications.create('notification.sisref.alert', {
    type: 'basic',
    title: 'Alerta para o SISREF',
    message: 'Não esqueça de registrar a frequência no SISREF!',
    iconUrl: 'alert.png'
  })
}

function createNotificationReminder () {
  chrome.notifications.create('notification.sisref.reminder', {
    type: 'basic',
    title: 'Lembrete para o SISREF',
    message: 'Antes de sair não esqueça de registrar a frequência no SISREF.',
    iconUrl: 'alert.png'
  })
}

chrome.notifications.onClicked.addListener(notificationId => {
  if (notificationId === 'notification.sisref.alert' || notificationId === 'notification.sisref.registrationPermission') {
    chrome.tabs.create({ url: sisrefUrl, active: true })
  }
})
