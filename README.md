# Sisref Reminder
> Extensão do Google Chrome para alertar os servidores da Defensoria Pública do Tocantins do status de registro de frequência.

__Author__: Lucivaldo Castro
__Email__: lucivaldocb@gmail.com

## Observação:

Este projeto é um projeto particular da Defensoria Pública, não coleta nenhum tipo de dado do servidor e não armazena dados em nenhum banco de dados.